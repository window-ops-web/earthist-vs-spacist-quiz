# Earthist vs Spacist Quiz

 This quiz is designed to assess your perspective on the Earthist vs. Spacist debate. Earthism and Spacism are not separate ideologies, but rather they are connected to and influenced by other belief systems and ideologies, such as political, economic, and environmental views.

 Earthism is a belief system that prioritizes the preservation and sustainable development of Earth, often aligning with progressive, environmentalist, and internationalist ideologies. Spacism, on the other hand, prioritizes space exploration and the colonization of other planets, which may be influenced by more technologically-driven, expansionist, or individualist ideologies.
 